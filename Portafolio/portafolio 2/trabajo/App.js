import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import * as ImagePicker from 'expo-image-picker';

// Importaciones de componentes personalizados
import Button from './components/Button';
import ImageViewer from './components/ImageViewer';
import CircleButton from './components/CircleButton';
import IconButton from './components/IconButton';
import EmojiPicker from "./components/EmojiPicker";

// Imagen por defecto
const PlaceholderImage = require('./assets/yone.jpg');

export default function App() {
  // Estados
  // Modal de emojis
  const [isModalVisible, setIsModalVisible] = useState(false);
  // Opciones de la app
  const [showAppOptions, setShowAppOptions] = useState(false);
  // URI de la imagen seleccionada
  const [selectedImage, setSelectedImage] = useState(null);

  // Función para seleccionar una imagen
  const pickImageAsync = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      quality: 1,
    });

    if (!result.canceled) {
      // Si se seleccionó una imagen
      setSelectedImage(result.assets[0].uri);
      setShowAppOptions(true);
    } else {
      alert('You did not select any image.');
    }
  };

  // Función para reiniciar
  const onReset = () => {
    setShowAppOptions(false);
  };

  // Función para agregar stickers
  const onAddSticker = () => {
    setIsModalVisible(true);
  };

  // Función para guardar la imagen
  const onSaveImageAsync = async () => {

  };
  
  // Función para cerrar el modal de emojis
  const onModalClose = () => {
    setIsModalVisible(false);
  };

  // Renderizado de la interfaz
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <ImageViewer
          placeholderImageSource={PlaceholderImage}
          selectedImage={selectedImage}/>
      </View>
      {showAppOptions ? (
        <View style={styles.optionsContainer}>
          <View style={styles.optionsRow}>
            <IconButton icon="refresh" label="Reset" onPress={onReset} />
            <CircleButton onPress={onAddSticker} />
            <IconButton icon="save-alt" label="Save" onPress={onSaveImageAsync} />
          </View>
        </View>
      ) : (
        <View style={styles.footerContainer}>
          <Button theme="primary" label="Choose a photo" onPress={pickImageAsync} />
          <Button label="Use this photo" onPress={() => setShowAppOptions(true)} />
        </View>
      )}
       <EmojiPicker isVisible={isModalVisible} onClose={onModalClose}>
        {}
      </EmojiPicker>
      <StatusBar style="auto
" />
</View>
);
}

//estilos
const styles = StyleSheet.create({
container: {
flex: 1,
backgroundColor: '#F2DFDF',
alignItems: 'center',
},
imageContainer: {
flex: 1,
paddingTop: 58,
},
footerContainer: {
flex: 1 / 3,
alignItems: 'center',
},
optionsContainer: {
position: 'absolute',
bottom: 80,
},
optionsRow: {
alignItems: 'center',
flexDirection: 'row',
},
});
