import { StyleSheet, Image } from 'react-native';

// Componente ImageViewer
export default function ImageViewer({ placeholderImageSource, selectedImage }) {
  // Determina la fuente de la imagen a mostrar
  const imageSource = selectedImage ? { uri: selectedImage } : placeholderImageSource;

  // Renderiza la imagen
  return <Image source={imageSource} style={styles.image} />;
}

// Estilos 
const styles = StyleSheet.create({
  image: {
    width: 320,
    height: 440,
    borderRadius: 18,
  },
});
