import { StyleSheet, View, Pressable, Text } from 'react-native';
import FontAwesome from "@expo/vector-icons/FontAwesome";

// Función para crear un botón personalizado
export default function Button({ label, theme, onPress }) {

  // Función para renderizar el botón con estilo "primary"
  const renderPrimaryButton = () => (
    <View style={[styles.buttonContainer, { borderWidth: 4, borderColor: "#8C566A", borderRadius: 18 }]}>
      <Pressable style={[styles.button, { backgroundColor: '#E6DFD9' }]} onPress={onPress}>
        <FontAwesome name="picture-o" size={18} color="#8C566A" style={styles.buttonIcon} />
        <Text style={[styles.buttonLabel, { color: "#8C566A" }]}>{label}</Text>
      </Pressable>
    </View>
  );

  // Renderizado del botón
  if (theme === "primary") {
    return renderPrimaryButton();
  }

  // Renderizado por defecto
  return (
    <View style={styles.buttonContainer}>
      <Pressable style={styles.button} onPress={onPress}>
        <Text style={styles.buttonLabel}>{label}</Text>
      </Pressable>
    </View>
  );
}

// Estilos para los botones
const styles = StyleSheet.create({
  buttonContainer: {
    width: 320,
    height: 68,
    marginHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 3,
  },
  button: {
    borderRadius: 10,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  buttonIcon: {
    paddingRight: 8,
  },
  buttonLabel: {
    color: '#72828C',
    fontSize: 16,
  },
});
