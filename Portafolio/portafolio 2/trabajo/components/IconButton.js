import { Pressable, StyleSheet, Text } from 'react-native';
import MaterialIcons from '@expo/vector-icons/MaterialIcons';

// Componente IconButton
export default function IconButton({ icon, label, onPress }) {
  // Renderiza un botón con ícono y texto
  return (
    <Pressable style={styles.iconButton} onPress={onPress}> 
      <MaterialIcons name={icon} size={24} color="#72828C" /> 
      <Text style={styles.iconButtonLabel}>{label}</Text> 
    </Pressable>
  );
}

// Estilos para el componente
const styles = StyleSheet.create({
  iconButton: {
    justifyContent: 'center', 
    alignItems: 'center', 
  },
  iconButtonLabel: {
    color: '#72828C', 
    marginTop: 12,    
  },
});
