import { StatusBar } from 'expo-status-bar'; 
import { StyleSheet, Text, View } from 'react-native'; 

// Función principal de la App
export default function App() {
  // Renderiza el componente `Navigation`
  return (
    <Navigation />
  );
}

