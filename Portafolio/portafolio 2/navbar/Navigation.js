import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

// componentes de pantalla
import HomeScreen from './screens/HomeScreen';
import Profile from './screens/Profile';
import SettingScreen from './screens/SettingScreen';
import StackScreen from './screens/StackScreen';

// Crea navegadores
const Tab = createBottomTabNavigator(); // Navegador de pestañas inferior
const Stack = createNativeStackNavigator(); // Navegador de pila

// Define la pila 'MyStack'
function MyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="HomeScreen" component={HomeScreen} /> 
      <Stack.Screen name="Stack" component={StackScreen} />
    </Stack.Navigator>
  );
}

// Define el tab principal 'MyTab'
function MyTab() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={MyStack} /> 
      <Tab.Screen name="Settings" component={SettingScreen} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
}

// Componente principal de navegación 'Navigation'
export default function Navigation() {
  return (
    <NavigationContainer>
      <MyTab />
    </NavigationContainer>
  );
}
