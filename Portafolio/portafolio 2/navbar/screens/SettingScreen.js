import React from "react";
import { View, Text, StyleSheet } from 'react-native';

// Función componente `SettingScreen`
export default function SettingScreen() {
  // Renderiza un contenedor `View` con estilo
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Hola SettingScreen</Text>
    </View>
  );
}

// Estilos
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 30
  }
});
