import React from "react";
import { View, Text, StyleSheet } from 'react-native';

// Función componente `Profile`
export default function Profile() {
  // Renderiza un contenedor `View` con estilo
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Hola Profile</Text>
    </View>
  );
}

// Estilo
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 30
  }
});
