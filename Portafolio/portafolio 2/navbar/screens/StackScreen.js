import React from "react";
import { View, Text, StyleSheet } from 'react-native';

// Función componente `StackScreen` (Probablemente no sea el nombre correcto)
export default function StackScreen() {
  // Renderiza un contenedor `View` sin estilo específico
  return (
    <View> 
      <Text style={styles.text}>Hola StackScreen</Text>
    </View>
  );
}

// Estilo
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 30
  }
});
