//IMPORTS
//Importa el componente StatusBar de la librería Expo para administrar la barra de estado del dispositivo y el StyleSheet los componentes básicos de React Native
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
//Importa Pokedex de la carpeta components
import Pokedex from './components/Pokedex'

export default function App() {
  // Renderiza la interfaz
  return (
    <View style={styles.container}>
      {/* Muestra la Pokedex */}
      <Pokedex />
      {/* El auto es para ajusta la barra de estado automáticamente */}
      <StatusBar style="auto" /> 
    </View>
  );
}

// Estilos
const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#fff', 
    alignItems: 'center', 
    justifyContent: 'center',
  },
});