// Importaciones
import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, FlatList, ActivityIndicator } from 'react-native';

// URL de la API de la pokedex 
const url = "https://pokeapi.deno.dev/pokemon";

export default function Pokedex() {

  // Estado del componente
  // Almacena datos de Pokemon
  const [data, setData] = useState(null); 
   // Almacena errores
  const [error, setError] = useState(null);
  // Indica si la carga está en curso
  const [isLoading, setIsLoading] = useState(true); 

  // Efecto secundario para obtener datos
  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then((result) => {
        setIsLoading(false);
        setData(result);
      }, (error) => {
        setIsLoading(false);
        setError(error);
      });
  }, []);

  console.log(data);

  // Función para determinar el contenido
  const getContent = () => {
    if (isLoading) {
      return (
        <View>
          <Text style={styles.textProps}>Cargando datos...</Text>
          <ActivityIndicator size="large" color="pink" />
        </View>
      );
    }
    if (error) {
      return <Text style={styles.textProps}>Error: {error}</Text>;
    }
    return (
      <View>
        <FlatList
          data={data}
          renderItem={({ item }) => (
            <View>
              <Image style={styles.image} source={{ uri: item.imageUrl }} />
              <Text>Nombre: {item.name}</Text>
              <Text>Género: {item.genus}</Text>
              <Text>Tipos: {item.types}</Text>
            </View>
          )}
        />
      </View>
    );
  };

  // Renderizado
  return (
    <View>
      {getContent()}
    </View>
  );
}

// Estilos
const styles = StyleSheet.create({
  textProps: {
    fontSize: 32,
    fontFamily: 'Gill Sans',
  },
  image: {
    width: 100,
    height: 100,
  },
});
