import React from 'react';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';
import AppNavigation from './src/navigations/AppNavigation';
import { SERVER_IP } from './src/config';

const App = () => {
  let [fontsLoaded] = useFonts({
    'Gilroy-ExtraBold': require('./src/assets/fonts/Gilroy-ExtraBold.otf'),
    'Gilroy-Light': require('./src/assets/fonts/Gilroy-Light.otf'),
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <AppNavigation />
  );
}

export default App;
