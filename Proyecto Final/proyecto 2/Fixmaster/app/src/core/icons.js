const arrowLeft = require("../assets/icons/arrow-left.png");
const mantenimiento = require("../assets/icons/mantenimiento.png");
const mantenimientoOutline = require("../assets/icons/mantenimiento-outline.png")
const solicitud = require("../assets/icons/solicitud.png");
const solicitudOutline = require("../assets/icons/solicitud-outline.png");
const herramienta = require("../assets/icons/herramienta.png")
const herramientaOutline = require("../assets/icons/herramienta-outline.png")
const tarea = require("../assets/icons/tarea.png")
const tareaOutline = require("../assets/icons/tarea-outline.png")


export default {
    arrowLeft,
    herramienta,
    herramientaOutline,
    solicitud,
    solicitudOutline,
    mantenimiento,
    mantenimientoOutline,
    tarea,
    tareaOutline,
}