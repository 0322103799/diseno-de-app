import { View, Text, Image } from "react-native";
import React from "react";
import { DrawerItemList, createDrawerNavigator } from "@react-navigation/drawer";
import { COLORS } from "../core";
import { Ionicons, AntDesign, Feather } from "@expo/vector-icons";
import BottomTabNavigation from "./BottomTabNavigation";
import {
  MantenimientosScreen,
  HerramientasScreen,
  SolicitudesScreen,
} from "../screens";
import { SafeAreaView } from "react-native-safe-area-context";

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator

      drawerContent={
        (props)=>{
            return (
                <SafeAreaView>
                    <View style={{
                        height: 200,
                        width: "100%",
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: COLORS.white
                    }}>
                        <Text style={{
                            fontSize: 18,
                            fontWeight: "bold",
                            color: COLORS.black,
                            marginBottom: 6
                        }}>Isabella Joanna</Text>
                        <Text style={{
                            fontSize: 16,
                            color: COLORS.black

                        }}>Product Designer</Text>
                    </View>
                    <DrawerItemList {...props} />
                </SafeAreaView>
            )
        }
      }
      screenOptions={{
        drawerStyle: {
          backgroundColor: COLORS.white,
          width: 250,
        },
        headerStyle: {
          backgroundColor: COLORS.white,
        },
        headerShown: false,
        headerTintColor: COLORS.black,
        drawerLabelStyle: {
          color: COLORS.black,
          fontSize: 14,
          marginLeft: -10,
        },
      }}
    >
      <Drawer.Screen
        name="Home"
        options={{
          drawerLabel: "Home",
          title: "Home",
          headerShadowVisible: false,
          drawerIcon: () => (
            <Ionicons name="home-outline" size={24} color={COLORS.black} />
          ),
        }}
        component={BottomTabNavigation}
      />
      <Drawer.Screen
        name="HerramientasScreen"
        options={{
          drawerLabel: "HerramientasScreen",
          title: "HerramientasScreen",
          headerShadowVisible: false,
          drawerIcon: () => (
            <Ionicons name="gift-outline" size={24} color={COLORS.black} />
          ),
        }}
        component={HerramientasScreen}
      />
      <Drawer.Screen
        name="SolicitudesScreen"
        options={{
          drawerLabel: "SolicitudesScreen",
          title: "SolicitudesScreen",
          headerShadowVisible: false,
          drawerIcon: () => (
            <Ionicons name="search-outline" size={24} color={COLORS.black} />
          ),
        }}
        component={SolicitudesScreen}
      />
      <Drawer.Screen
        name="MantenimientosScreen"
        options={{
          drawerLabel: "MantenimientosScreen",
          title: "MantenimientosScreen",
          headerShadowVisible: false,
          drawerIcon: () => (
            <Ionicons name="heart-outline" size={24} color={COLORS.black} />
          ),
        }}
        component={MantenimientosScreen}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
