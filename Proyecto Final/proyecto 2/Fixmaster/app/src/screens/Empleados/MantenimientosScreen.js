import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import Background from '../../components/Background';
import BackButton from '../../components/BackButton';
import Header from '../../components/Header';
import TextInput from '../../components/TextInput';

const MantenimientosScreen = ({navigation}) => {
  const [maintenanceType, setMaintenanceType] = useState('');
  const [date, setDate] = useState('');
  const [place, setPlace] = useState('');
  const [machineNumber, setMachineNumber] = useState('');
  const [maintenanceDescription, setMaintenanceDescription] = useState('');

  const acceptMaintenance = () => {
    // Lógica para aceptar el mantenimiento
    // Aquí deberías incluir lo que sucede cuando el botón es presionado, por ejemplo, enviar los datos a una API o navegar a otra pantalla
    console.log({
      maintenanceType,
      date,
      place,
      machineNumber,
      maintenanceDescription,
    });
  };

  return (
    <Background>
    <Header>Mantenimiento</Header>
    <View style={styles.container}>
      <TextInput
        placeholder="Tipo de mantenimiento"
        style={styles.input}
        value={maintenanceType}
        onChangeText={setMaintenanceType}
      />
      <TextInput
        placeholder="Fecha"
        style={styles.input}
        value={date}
        onChangeText={setDate}
      />
      <TextInput
        placeholder="Lugar"
        style={styles.input}
        value={place}
        onChangeText={setPlace}
      />
      <TextInput
        placeholder="Número de la máquina"
        style={styles.input}
        value={machineNumber}
        onChangeText={setMachineNumber}
      />
      <TextInput
        placeholder="Descripción del mantenimiento"
        style={styles.input}
        value={maintenanceDescription}
        onChangeText={setMaintenanceDescription}
        multiline
      />
      <TouchableOpacity style={styles.button} onPress={acceptMaintenance}>
        <Text style={styles.buttonText}>Aceptar</Text>
      </TouchableOpacity>
    </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#fff',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  button: {
    backgroundColor: 'green',
    padding: 15,
    borderRadius: 5,
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default MantenimientosScreen;
