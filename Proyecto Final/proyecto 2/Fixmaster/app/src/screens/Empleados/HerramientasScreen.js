import React, { useState } from 'react';
import { StyleSheet, View, Text, Button, FlatList, TouchableOpacity } from 'react-native';
import Background from '../../components/Background';
import BackButton from '../../components/BackButton';
import Header from '../../components/Header';

const HerramientasScreen = ({ onConfirm, navigation }) => {
  const [availableTools, setAvailableTools] = useState([{ id: '1', name: 'Martillo' }, { id: '2', name: 'Destornillador' }]); // Ejemplo de herramientas disponibles
  const [addedTools, setAddedTools] = useState([]);

  const addTool = (tool) => {
    if (!addedTools.includes(tool)) {
      setAddedTools([...addedTools, tool]);
    }
  };

  const removeTool = (tool) => {
    setAddedTools(addedTools.filter(t => t !== tool));
  };

  return (
    <Background>
    <Header>Herramientas</Header>
    <View style={styles.container}>
      <FlatList
        data={availableTools}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
            <Text>{item.name}</Text>
            <Button title="Agregar" onPress={() => addTool(item)} />
          </View>
        )}
        keyExtractor={item => item.id}
      />
      <FlatList
        data={addedTools}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
            <Text>{item.name}</Text>
            <Button title="Quitar" onPress={() => removeTool(item)} />
          </View>
        )}
        keyExtractor={item => item.id}
      />
      <Button title="Confirmar" onPress={onConfirm} />
    </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
  backButton: {
    marginBottom: 10,
  },
});

export default HerramientasScreen;
