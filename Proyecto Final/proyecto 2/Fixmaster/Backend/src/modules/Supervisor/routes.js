const express = require('express');

const response = require('../../connect/response');
const driver = require('./index');





const router = express.Router();

router.get('/', all);
router.get('/:id',selectSupervisor);
router.put('/', disable);
router.post('/', add);

async function all(req, res, next) {
    try {
        const items = await driver.all();
        response.success(req, res, items, 200);
    }
    catch (error) {
        next(error);
    }
};

// async function one(req, res, next) {
//     try{
//         const id = req.params.id;
//         const item = await driver.one(id);
//         response.success(req, res, item, 200);
//     }catch(error){
//         next(error);
//     }
// };

async function add(req, res, next){
    try{
        const items = await driver.add(req.body);
        if(req.body.id == 0){
            response.success(req, res, 'Item agregado', 200);
        } else {
            response.success(req, res, 'Item actualizado', 200);
        }
    }catch(error){
        next();
    }
}


async function disable (req, res,next){
    try{
        const items = await driver.disable(req.body);
        response.success(req, res, 'Item desactivado', 200);
    }catch(error){
        next();
    }
}

async function selectSupervisor(req, res, next) {
    try {
        const items = await driver.selectSupervisor();
        response.success(req, res, items, 200);
    }
    catch (error) {
        next(error);
    }
}


module.exports = router;