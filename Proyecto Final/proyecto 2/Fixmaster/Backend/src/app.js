const express = require('express');
const morgan = require('morgan');
const config = require('./config');
const error = require('./connect/errors');
const cors = require('cors');

const users = require('./modules/users/routes');
const auth = require('./modules/auth/routes');
const Depto = require('./modules/Departamento/routes');
const Herra = require('./modules/Herramienta/routes');
const Tarea = require('./modules/Tarea/routes');
const SoliHerra = require('./modules/SoliHerra/routes');
const Reporte = require('./modules/Reporte/routes');
const Maquina = require('./modules/Maquina/routes');
const Problema = require('./modules/Problema/routes');
const Solicitud = require('./modules/Solicitud/routes');
const Supervisor = require('./modules/Supervisor/routes');

const app = express();

// Middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());
app.use (express.urlencoded({ extended: true }));

// Configuracion
app.set('port', config.app.port);

// routes
app.use('/api/users', users);
app.use('/api/auth', auth);
app.use('/api/Depto', Depto);
app.use('/api/Herra', Herra);
app.use('/api/Tarea', Tarea);
app.use('/api/SoliHerra', SoliHerra);
app.use('/api/Reporte', Reporte);
app.use('/api/Maquina', Maquina);
app.use('/api/Problema', Problema);
app.use('/api/Solicitud', Solicitud);
app.use('/api/Supervisor', Supervisor);



app.use(error);

module.exports = app;








