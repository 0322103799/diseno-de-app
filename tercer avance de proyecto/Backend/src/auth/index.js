const jwt = require('jsonwebtoken');
config = require('../config');

const secret = config.jwt.secret;

function giveToken(data) {
    return jwt.sign(data, secret);
}

function verifyToken(token) {
    return jwt.verify(token, secret);
}

const checkToken = {
    confirmToken: function(req) {
        const decoded = decodeHeaders(req);
    }
}

function getToken(Authorization) {
    console.log(Authorization); 
    if(!Authorization) {
        throw new Error('Sin token');
    }
    
    if(Authorization.indexOf('Bearer') === -1) {
        throw new Error('Formato invalido');
    }

    let token = Authorization.replace('Bearer ', '');
    return token;
}


function decodeHeaders(req) {
    console.log(req.headers); 
    const Authorization = req.headers.authorization || '';
    const token = getToken(Authorization);
    const decoded = verifyToken(token);

    req.user = decoded;

    return decoded;
}

module.exports = {
    giveToken,
    checkToken,
}
