const response = require('./response');

function error(err,req, res, next) {
    console.error('[red error]', err);

    const message = err.message || 'Error interno del servidor';
    const status = err.statuscode || 500;

    response.error(req, res, message, status);
}
module.exports = error;