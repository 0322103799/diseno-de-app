import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { useFonts } from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import AppNavigation from './src/navigation/AppNavigation';
import WebScreen from './src/screens/WebScreen';
import { SafeAreaProvider } from 'react-native-safe-area-context';

SplashScreen.preventAutoHideAsync();

export default function App() {
  const [fontsLoaded] = useFonts({
    'Gilroy-Light': require('./src/assets/fonts/Gilroy-Light.otf'),
    'Gilroy-ExtraBold': require('./src/assets/fonts/Gilroy-ExtraBold.otf'),
    'Gilroy-Bold': require('./src/assets/fonts/Gilroy-Bold.ttf'),
    'Gilroy-Regular': require('./src/assets/fonts/Gilroy-Regular.ttf'),
  });
  const [appIsReady, setAppIsReady] = useState(false);

  useEffect(() => {
    async function prepare() {
      try {
        await SplashScreen.preventAutoHideAsync();
      } catch (e) {
        console.warn(e);
      } finally {
        if (fontsLoaded) {
          setAppIsReady(true);
        }
      }
    }

    prepare();
  }, [fontsLoaded]);

  useEffect(() => {
    if (appIsReady) {
      SplashScreen.hideAsync();
    }
  }, [appIsReady]);

  if (!appIsReady) {
    return null;
  }

  // Verifica si la plataforma es 'web'
  if (Platform.OS === 'web') {
    return <WebScreen />;
  }

  return (
    <SafeAreaProvider>
    <View style={styles.container}>
      <AppNavigation />
    </View>
    </SafeAreaProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
