import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import Axios from 'axios';
import RNPickerSelect from 'react-native-picker-select';
import { SERVER_IP } from '../../../config';

const deptoURL = `http://${SERVER_IP}:8000/api/Depto`;
const supervisorURL = `http://${SERVER_IP}:8000/api/Supervisor`;
const usersURL = `http://${SERVER_IP}:8000/api/users`;

export default class AsignarDepartamentoSupervisor extends Component {
  state = {
    departamentos: [],
    departamentoSeleccionado: null,
    supervisoresDepartamento: [],
  };

  componentDidMount() {
    this.obtenerDepartamentos();
    this.obtenerSupervisores();
  }

  obtenerSupervisores = async () => {
    try {
      const response = await Axios.get(usersURL);
      const supervisoresFiltrados = response.data.body.filter(
        (supervisor) => supervisor.rol === 'supervisor'
      );
      this.setState({ supervisores: supervisoresFiltrados });
    } catch (error) {
      console.error('Error al obtener supervisores:', error);
    }
  };

  obtenerDepartamentos = async () => {
    try {
      const response = await Axios.get(deptoURL);
      this.setState({ departamentos: response.data.body });
    } catch (error) {
      console.error('Error al obtener departamentos:', error);
    }
  };

  compararSupervisorDepartamento = async () => {
    try {
      const response = await Axios.get(supervisorURL);
      const supervisoresDepartamento = response.data.body.filter(
        (supervisor) => supervisor.departamento_id === this.state.departamentoSeleccionado
      );
      console.log('supervisoresDepartamento:', response.data.body);
      this.setState({ supervisoresDepartamento });
    } catch (error) {
      console.error('Error al obtener supervisores:', error);
    }
  };

  compararSupervisorDepartamento = async () => {
    try {
      const response = await Axios.get(supervisorURL);
      const supervisoresDepartamento = response.data.body.filter(
        (supervisor) => supervisor.departamento_id === this.state.departamentoSeleccionado
      );
      console.log('supervisoresDepartamento:', supervisoresDepartamento);
      this.setState({ supervisoresDepartamento });
    } catch (error) {
      console.error('Error al obtener supervisores:', error);
    }
  };
  

  render() {
    return (
      <View style={styles.container}>
        <Text>Selecciona un departamento:</Text>
        <View style={styles.pickerContainer}>
          <RNPickerSelect
            style={pickerSelectStyles}
            placeholder={{
              label: 'Seleccione un departamento...',
              value: null,
            }}
            onValueChange={(value) => {
              this.setState({ departamentoSeleccionado: value });
              this.obtenerDepartamentos();
            }}
            items={this.state.departamentos.map((depto) => ({
              label: depto.nombre,
              value: depto.id,
            }))}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={this.asignarDepartamentoSupervisor}>
          <Text style={styles.buttonText}>Asignar Departamento</Text>
        </TouchableOpacity>

        <View style={styles.supervisoresContainer}>
          <Text>Supervisores con departamento asignado:</Text>
          <ScrollView>
            {this.state.supervisoresDepartamento.map((supervisor) => (
              <View key={supervisor.usuario_id} style={styles.supervisorItem}>
                <Text>{supervisor.nombre}</Text>
              </View>
            ))}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
  },
  pickerContainer: {
    width: '80%', // Ajusta el ancho al 80% de la pantalla
    marginVertical: 20, // Agrega margen vertical
  },
  button: {
    backgroundColor: '#007AFF',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
  },
  supervisoresContainer: {
    marginTop: 20,
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
  },
  supervisorItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginBottom: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ccc',
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30,
  },
});
