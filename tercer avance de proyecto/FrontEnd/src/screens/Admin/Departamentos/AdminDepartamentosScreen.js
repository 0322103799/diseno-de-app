import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from "react-native";
import Axios from 'axios';
import { SERVER_IP } from '../../../config';

const deptoURL = `http://${SERVER_IP}:8000/api/Depto`;
const supervisorURL = `http://${SERVER_IP}:8000/api/users`;

export default class AdmiDepartamentosScreen extends Component {
    state = {
        departamentos: [],
    };

    componentDidMount() {
        this.obtenerDepartamentos(); 
    }
    
    
    obtenerDepartamentos = async () => {
        try {
            const response = await Axios.get(deptoURL);
            this.setState({ departamentos: response.data.body });
        } catch (error) {
            console.error('Error al obtener departamentos:', error);
        }
    };

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <Text style={styles.header}>Administrar Departamentos</Text>
                <ScrollView style={styles.departamentosContainer}>
                    {this.state.departamentos.map((depto) => (
                        <View key={depto.id} style={styles.departamentoItem}>
                            <Text>{depto.nombre}</Text>
                            <TouchableOpacity style={styles.gestionarButton} onPress={() => navigation.navigate('Gestionar Departamento', { id: depto.id })}>
                                <Text style={styles.gestionarButtonText}>Gestionar</Text>
                            </TouchableOpacity>
                        </View>
                    ))}
                </ScrollView>
                <TouchableOpacity style={styles.button} onPress={() => console.log("Agregar nuevo departamento")}>
                    <Text style={styles.buttonText}>Agregar Nuevo Departamento</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Asignar Departamento')}>
                    <Text style={styles.buttonText}>Asignar Departamento a Supervisor</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#f5f5f5",
        padding: 20,
    },
    header: {
        fontSize: 22,
        fontWeight: "bold",
        marginBottom: 20,
    },
    button: {
        width: "85%",
        backgroundColor: "#007AFF",
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5,
        marginBottom: 10,
        alignItems: "center",
    },
    buttonText: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 16,
    },
    departamentosContainer: {
        width: "100%",
    },
    departamentoItem: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: "#fff",
        paddingHorizontal: 20,
        paddingVertical: 15,
        marginBottom: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#ccc",
    },
    gestionarButton: {
        backgroundColor: "#007AFF",
        paddingHorizontal: 15,
        paddingVertical: 8,
        borderRadius: 5,
    },
    gestionarButtonText: {
        color: "#fff",
        fontWeight: "bold",
        fontSize: 14,
    },
});
  