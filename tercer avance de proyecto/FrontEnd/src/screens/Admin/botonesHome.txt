        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Maquinas')}
        >
          <Text style={styles.buttonText}>Maquinas</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('Departamentos')}
        >
          <Text style={styles.buttonText}>Departamentos</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={styles.button}
          onPress={() => navigation.navigate('Estados')}
          >
          <Text style={styles.buttonText}>Tipos de estado</Text>
        </TouchableOpacity>