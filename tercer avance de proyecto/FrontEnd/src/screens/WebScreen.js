import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native"; // Añadido TouchableOpacity
import { SERVER_IP } from "../config";
import axios from "axios";

const WebScreen = ({ updateMaquinaState }) => {
  const [maquinas, setMaquinas] = useState([]);

  useEffect(() => {
    const fetchMaquinas = async () => {
      try {
        const response = await axios.get(`http://${SERVER_IP}:8000/api/Maquina`);
        if (response.data) {
          setMaquinas(response.data.body);
        }
      } catch (error) {
        console.error("Error al obtener las máquinas:", error);
      }
    };

    fetchMaquinas();
  }, []);

  // Función para actualizar el estado de una máquina específica
  const updateMaquinaEstado = (index, nuevoEstado) => {
    const nuevasMaquinas = [...maquinas];
    nuevasMaquinas[index].estatus = nuevoEstado;
    setMaquinas(nuevasMaquinas);
  };

  return (
    <View style={styles.container}>
      <View style={styles.tableHeader}>
        <Text style={styles.headerText}>Linea</Text>
        <Text style={styles.headerText}>Estatus</Text>
        <Text style={styles.headerText}>Número de Serie</Text>
        <Text style={styles.headerText}>Temperatura</Text>
        <Text style={styles.headerText}>Tiempo en producción</Text>
        <Text style={styles.headerText}>Tiempo muerto</Text>
      </View>
      {maquinas.map((maquina, index) => (
        <TouchableOpacity // Cambiado View por TouchableOpacity
          key={index}
          style={[
            styles.tableRow,
            { backgroundColor: maquina.estatus === "MP" ? "gray" : 
                              maquina.estatus === "MT" ? "green" : 
                              maquina.estatus === "MER" ? "blue" : 
                              maquina.estatus === "EDM" ? "yellow" : 
                              maquina.estatus === "SP" ? "orange" : "transparent" }
          ]}
          onPress={() => updateMaquinaEstado(index, "MP")}
        >
          <Text style={styles.rowText}>{maquina.nombre}</Text>
          <Text style={styles.rowText}>{maquina.estatus}</Text>
          <Text style={styles.rowText}>{maquina.numeroserie}</Text>
          <Text style={styles.rowText}>{maquina.temperatura}</Text>
          <Text style={styles.rowText}>{maquina.tiempoProduccion}</Text>
          <Text style={styles.rowText}>{maquina.tiempoMuerto}</Text>
        </TouchableOpacity>
      ))}
      {/* Simbología */}
      <View style={styles.symbolContainer}>
        <View style={[styles.symbolItem, { backgroundColor: "gray" }]} />
        <Text style={styles.symbolText}>MP - Mantenimiento Preventivo</Text>
        <View style={{ width: 8 }} /> 
        <View style={[styles.symbolItem, { backgroundColor: "green" }]} />
        <Text style={styles.symbolText}>MT - Maquina Trabajando</Text>
        <View style={{ width: 8 }} /> 
        <View style={[styles.symbolItem, { backgroundColor: "blue" }]} />
        <Text style={styles.symbolText}>MER - Maquina en Reparacion</Text>
        <View style={{ width: 8 }} /> 
        <View style={[styles.symbolItem, { backgroundColor: "yellow" }]} />
        <Text style={styles.symbolText}>EDM - Espera de Material</Text>
        <View style={{ width: 8 }} /> 
        <View style={[styles.symbolItem, { backgroundColor: "orange" }]} />
        <Text style={styles.symbolText}>SP - Sin Operador</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 20,
    position: "relative",
  },
  tableHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomWidth: 4,
    borderBottomColor: "#ccc",
    marginBottom: 10,
    paddingVertical: 15,
  },
  tableRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  headerText: {
    fontFamily: "Gilroy-Bold",
    fontSize: 16,
    width: "20%",
    textAlign: "center",
    borderRightWidth: 1,
    borderRightColor: "#ccc",
  },
  rowText: {
    fontFamily: "Gilroy-Regular",
    fontSize: 16,
    width: "20%",
    textAlign: "center",
    borderRightWidth: 1,
    borderRightColor: "#ccc",
  },
  symbolContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  symbolItem: {
    width: 16,
    height: 16,
    marginRight: 4,
    borderRadius: 2,
  },
  symbolText: {
    fontFamily: "Gilroy-Bold",
    fontSize: 14,
    marginLeft: 4,
  },
});

export default WebScreen;
