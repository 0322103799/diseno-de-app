import axios from 'axios';

// Función para enviar mensaje vía WhatsApp usando la API de Facebook
export const sendMessageViaWhatsApp = async (taskName, departamentos, machineNumber, description) => {
  const options = {
    method: 'POST',
    url: 'https://graph.facebook.com/v18.0/313149311872626/messages',
    headers: {
      'Authorization': 'Bearer EAAGPfUvIelEBO0zsxvpTX8A8KCHbK0R16CpEdZAl3Fbjd13wCid4M7hrOMQEhnoOupAvsiJ1A2NopGcZBlckGGSviGuZBNdYuBVNO1eJwsggVBryZC9TMUH2eZB6TDioxZAaRZB1aSvpvjs8zgDW3dk32wzMcM9ZCW8fxn4FIw9ZArIGZCWgjYVMtZCNtPPOW5UDm5AIwvUIBu0LxlW9hZCW0E6u1T4Mpn8a6L9fNOXJ01QZD',
      'Content-Type': 'application/json'
    },
    data: {
      messaging_product: "whatsapp",
      to: "526648537685", // Número de teléfono de destino
      type: "template",
      template: {
        name: "aviso", // Nombre de la plantilla registrada
        language: {
          code: "en_US" // Código de idioma de la plantill
        },
        components: [
          {
            type: "body",
            parameters: [
              {
                type: "text",
                text: taskName
              },
              {
                type: "text",
                text: departamentos
              },
              {
                type: "text",
                text: machineNumber
              },              {
                type: "text",
                text: description
              }
            ]
          }
        ]
      }
    }
  };

  try {
    const response = await axios(options);
    console.log('Mensaje enviado:');
  } catch (error) {
  }
};

sendMessageViaWhatsApp();