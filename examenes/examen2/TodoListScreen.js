import React, { useState, useEffect } from 'react';
import { ScrollView, Text, StyleSheet } from 'react-native';
import { fetchTodos } from './apiService'; 

const TodoListScreen = ({ listFunction }) => {
  const [todos, setTodos] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const fetchedTodos = await fetchTodos();
      setTodos(fetchedTodos);
      setLoading(false);
    };

    fetchData();
  }, []);

  if (loading) {
    return <Text>Cargando...</Text>;
  }

  const renderList = () => {
    const listItems = listFunction(todos);
    return listItems.map((item, index) => {
      if (typeof item === 'number') {
        return (
          <Text key={index} style={styles.listItem}>
            ID: {item}
          </Text>
        );
      } else if (item.title && item.id) {
        return (
          <Text key={index} style={styles.listItem}>
            {item.id} - {item.title}
          </Text>
        );
      } else if (item.userId && item.id) {
        return (
          <Text key={index} style={styles.listItem}>
            ID: {item.id}, UserID: {item.userId}
          </Text>
        );
      }
      return null; 
    });
  };

  return (
    <ScrollView style={styles.container}>
      {renderList()}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#cccccc',
  },
});

export default TodoListScreen;
