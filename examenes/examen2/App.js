import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TodoListScreen from './TodoListScreen';
import { getAllIds } from './allids';
import { getAllIdsTitles } from './allidstitles';
import { getAllUnresolvedIdsTitles } from './allunresolvedidtitles';
import { getAllResolvedIdsTitles } from './allresolvedidtitles';
import { getAllIdsUserIds } from './allidsuserid';
import { getAllResolvedIdsUserIds } from './allresolvediduserid';
import { getAllUnresolvedIdsUserIds } from './allunresolvediduserid';

const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Todos los IDs" children={() => <TodoListScreen listFunction={getAllIds} />} />
        <Tab.Screen name="IDs y Títulos" children={() => <TodoListScreen listFunction={getAllIdsTitles} />} />
        <Tab.Screen name="Sin Resolver" children={() => <TodoListScreen listFunction={getAllUnresolvedIdsTitles} />} />
        <Tab.Screen name="Resueltos" children={() => <TodoListScreen listFunction={getAllResolvedIdsTitles} />} />
        <Tab.Screen name="IDs y UserID" children={() => <TodoListScreen listFunction={getAllIdsUserIds} />} />
        <Tab.Screen name="Resueltos UserID" children={() => <TodoListScreen listFunction={getAllResolvedIdsUserIds} />} />
        <Tab.Screen name="Sin Resolver UserID" children={() => <TodoListScreen listFunction={getAllUnresolvedIdsUserIds} />} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
