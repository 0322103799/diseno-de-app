export const getAllIdsTitles = (todos) => todos.map(todo => ({ id: todo.id, title: todo.title }));
