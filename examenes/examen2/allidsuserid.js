export const getAllIdsUserIds = (todos) => todos.map(todo => ({ id: todo.id, userId: todo.userId }));
