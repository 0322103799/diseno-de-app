export const fetchTodos = async () => {
  try {
    const response = await fetch('http://jsonplaceholder.typicode.com/todos');
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    return await response.json();
  } catch (error) {
    console.error('Error fetching todos:', error);
    return []; 
  }
};
