export const getAllUnresolvedIdsUserIds = (todos) => 
  todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, userId: todo.userId }));

  