export const getAllUnresolvedIdsTitles = (todos) => 
  todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, title: todo.title }));
