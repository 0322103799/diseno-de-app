export const getAllIds = (todos) => todos.map(todo => todo.id);
